FROM openjdk:8-jre-alpine
COPY target/checkstyle-report-pipe-shaded.jar /checkstyle-report-pipe.jar
COPY LICENSE.txt README.md /

ENTRYPOINT ["java", "-jar", "/checkstyle-report-pipe.jar"]
