# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.5.2

- patch: Internal maintenance: update elease process for multiplatform build.

## 0.5.1

- patch: Internal maintenance: Update  release process.

## 0.5.0

- minor: Bump version of jackson-databind to 2.17.2 to fix security issues.

## 0.4.0

- minor: Fix duplicate annotation id error when attempting to submit more than one annotation per-line.
- patch: Bump version of jackson-databind to 2.13.4 to fix security issues.

## 0.3.2

- patch: Internal maintenance: Internal maintenance: update default-image and release process.
- patch: Internal maintenance: bump violations-lib to fix fail if line attribute is not present.
- patch: Internal maintenance: update community link.

## 0.3.1

- patch: Internal maintenance: Bump versions of jackson-databind to 2.10.5.1 and jackson-annotations to 2.10.5

## 0.3.0

- minor: Support configurable title of checkstyle report.
- patch: Add reporter field to report payload

## 0.2.1

- patch: Only relativize file paths that are absolute

## 0.2.0

- minor: Fix default values for pipe variables

## 0.1.6

- patch: bugfix

## 0.1.5

- patch: add debug env variable DEBUG that lists the request and responses from bitbucket.org

## 0.1.4

- patch: Fix example descriptions in the Readme.

## 0.1.3

- patch: correct quoting in pipe.yml

## 0.1.2

- patch: changes to pipe.yml

## 0.1.1

- patch: update pipe variables

## 0.1.0

- minor: Add configuration to only fail Code Insight report when certain severity of violations occur
- minor: Sort annotations by severity to submit the more severe ones first.

## 0.0.2

- patch: Documentation and build updates

## 0.0.1

- patch: Initial version
