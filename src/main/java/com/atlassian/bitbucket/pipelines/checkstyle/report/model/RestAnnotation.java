/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report.model;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.immutables.value.Value;


/**
 * An annotation to be placed against a report.
 */
@Value.Immutable
@JsonDeserialize(builder = ImmutableRestAnnotation.Builder.class)
@JsonPOJOBuilder(withPrefix = "set")
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class RestAnnotation {

    /**
     * The type of the annotation.
     */
    public enum Type {
        VULNERABILITY,
        CODE_SMELL,
        BUG,
        @JsonEnumDefaultValue
        UNKNOWN
    }

    /**
     * The severity of the annotation.
     */
    public enum Severity {
        HIGH,
        MEDIUM,
        LOW,
        CRITICAL,
        @JsonEnumDefaultValue
        UNKNOWN
    }

    /**
     * @return the external id of the annotation
     */
    @JsonProperty("external_id")
    public abstract String getExternalId();

    /**
     * @return the optional type of the annotation
     */
    @JsonProperty("annotation_type")
    public abstract Type getAnnotationType();

    /**
     * @return the optional path to place the annotation against in the UI; if none is provided the annotation is just a part of the report only
     */
    public abstract String getPath();

    /**
     * @return the optional line to place the annotation against in the UI; if none is provided the annotation is just a part of the report only
     */
    public abstract Long getLine();

    /**
     * @return the summary of the annotation
     */
    public abstract String getSummary();

    /**
     * @return the optional details of the annotation
     */
    public abstract String getDetails();

    /**
     * @return the optional severity of the annotation
     */
    public abstract Severity getSeverity();

}
