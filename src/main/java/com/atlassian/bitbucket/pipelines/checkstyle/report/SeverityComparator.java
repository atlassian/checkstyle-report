/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report;

import java.util.Comparator;
import se.bjurr.violations.lib.model.Violation;

/**
 *
 * @author mkleint
 */
public class SeverityComparator implements Comparator<Violation> {

    @Override
    public int compare(Violation o1, Violation o2) {
        return - toInteger(o1).compareTo(toInteger(o2));
    }

    private Integer toInteger(Violation violation) {
        switch (violation.getSeverity()) {
            case ERROR : return 2;
            case WARN  : return 1;
            case INFO  : return 0;
            default    : return -1;
        }
    }

}
