/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report;

import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestAnnotation;
import se.bjurr.violations.lib.model.SEVERITY;
import se.bjurr.violations.lib.model.Violation;

/**
 *
 * @author purplechill
 */
class RestAnnotationUtil {

    static RestAnnotation.Severity toSeverity(SEVERITY severity) {
        switch (severity) {
            case ERROR:
                return RestAnnotation.Severity.HIGH;
            case WARN:
                return RestAnnotation.Severity.MEDIUM;
            case INFO:
                return RestAnnotation.Severity.LOW;
            default:
                return RestAnnotation.Severity.UNKNOWN;
        }
    }

    static String createExternalId(Violation t, String reportExternalId) {
        return reportExternalId + ":" + String.valueOf(t.hashCode());
    }
}
