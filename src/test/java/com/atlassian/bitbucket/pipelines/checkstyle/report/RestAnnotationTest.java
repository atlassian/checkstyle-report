/*
 * Copyright 2020 Atlassian.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.bitbucket.pipelines.checkstyle.report;

import com.atlassian.bitbucket.pipelines.checkstyle.report.model.ImmutableRestAnnotation;
import com.atlassian.bitbucket.pipelines.checkstyle.report.model.RestAnnotation;
import se.bjurr.violations.lib.model.SEVERITY;
import se.bjurr.violations.lib.model.Violation;
import se.bjurr.violations.lib.reports.Parser;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author purplechill
 */
public class RestAnnotationTest {

    public RestAnnotationTest() {
    }

    @org.junit.jupiter.api.Test
    public void testRestAnnotationUniqueExternalId() {
        // The following Violations are identical except for the column value
        List<Violation> violations = new ArrayList<Violation>() {
            {
            add(
                Violation.violationBuilder()
                .setCategory("cat")
                .setColumn(1)
                .setEndLine(1)
                .setFile("f.txt")
                .setMessage("x")
                .setReporter("r")
                .setStartLine(1)
                .setParser(Parser.CHECKSTYLE)
                .setSeverity(SEVERITY.INFO)
                .build());
            add(
                Violation.violationBuilder()
                .setCategory("cat")
                .setColumn(2)
                .setEndLine(1)
                .setFile("f.txt")
                .setMessage("x")
                .setReporter("r")
                .setStartLine(1)
                .setParser(Parser.CHECKSTYLE)
                .setSeverity(SEVERITY.INFO)
                .build());
            }
        };

        List<RestAnnotation> restViolations = violations.stream()
                .map((Violation v) -> {
                    return ImmutableRestAnnotation.builder()
                            .annotationType(RestAnnotation.Type.CODE_SMELL)
                            .details(v.getRule())
                            .externalId(RestAnnotationUtil.createExternalId(v, "ABC123"))
                            .line(v.getStartLine().longValue())
                            .path(v.getFile())
                            .severity(RestAnnotationUtil.toSeverity(v.getSeverity()))
                            .summary(v.getMessage())
                            .build();
                }).collect(Collectors.toList());

        assertNotEquals(restViolations.get(0).getExternalId(),
                        restViolations.get(1).getExternalId());
    }
}
